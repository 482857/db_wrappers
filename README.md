# Database Wrappers
This database contains docker image wrappers of vector databases. This is necessary in order to give databases sufficent permissions to run in rancher kubernetes cluster.
You can view different wrappers by swithcing the branches. Currently there are wrappers for the following vector databases:
- chromadb
- milvus
- qdrant
- weaviate
